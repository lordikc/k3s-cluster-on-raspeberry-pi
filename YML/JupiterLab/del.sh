#!/bin/bash

kubectl delete deploy -n jlab jupyterlab
kubectl delete svc -n jlab jupyterlab
kubectl delete pvc -n jlab jupyterlab-pvc
kubectl delete ns jlab
