
## Deploy Wordpress with TLS

In this chapter we will deploy a Wordpress Service published through Ingress and with HTTPS.

### Create the certificate for the Ingress

Create the key:

```bash
openssl genrsa -out wp.key 4096
```

Create a Certificate Signing Request:

```bash
openssl req -sha512 -new \
    -subj "/C=FR/L=Paris/O=Lordikc/CN=wp" \
    -key wp.key \
    -out wp.csr
```

Configure extensions for a server certificate:

```
cat > v3.ext <<-EOF
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
extendedKeyUsage = serverAuth
subjectAltName = @alt_names

[alt_names]
DNS.1=wp
DNS.2=wordpress
DNS.3=wp.localnet
IP.1=192.168.15.151
EOF
```

Sign the CSR with our CA:


```bash
openssl x509 -req -sha512 -days 3650 \
    -extfile v3.ext \
    -CA ca-ssl.crt -CAkey ca-ssl.key -CAcreateserial \
    -in wp.csr \
    -out wp.crt
openssl x509 -in wp.crt -text
```

Create a secret with the certificate:

```bash
kubectl create secret tls wordpress-tls --key="wp.key" --cert="wp.crt"
```

### Create a secret for the password of Wordpress and mysql:

The secret hold the password for mysql and wordpress.

```bash
echo -n my-secret-pass | base64
```

```yaml
---
### A secret to be used for several purposes:
### - Root password of mysql
### - wordpress mysql user password
apiVersion: v1
kind: Secret
metadata:
  name: mysql-pass
  labels:
    app: postgres
type: Opaque
data:
  password: bXktc2VjcmV0LXBhc3M=
```

### Deploy Wordpress


- wordpress-configmap.yml
- wordpress-secret.yml
- wordpress-ingress.yml
- wordpress-service.yml
- wordpress-statefulset.yml
- mariadb-wp-service.yml
- mariadb-wp-statefulset.yml


```bash
kubectl get svc wordpress-mysql -o jsonpath="{.status.loadBalancer.ingress[*].ip}"
```
 

### Import the content content

#### The Database
First update the content of the database with the new URL.

Example:
```
sed -e 's@https://192.168.0.10/wordpress/@https://wp/@g' wordpress.db > wordpress-new.db
```

Get the IP of the mysql service:
```
$ kubectl get svc wordpress-mysql
NAME              TYPE           CLUSTER-IP     EXTERNAL-IP       PORT(S)          AGE
wordpress-mysql   LoadBalancer   10.43.42.186   192.168.122.154   3306:30535/TCP   4h15m

$ kubectl get svc wordpress-mysql -o jsonpath="{.status.loadBalancer.ingress[*].ip}"
192.168.122.154
```

Import the content
```
mysql -h 192.168.122.154 wordpress -u wordpress -p<password> < wordpress-new.db
```

#### The upload content

Identify the wordpress pod running:
```
$ kubectl get pod
NAME                              READY   STATUS    RESTARTS      AGE
wordpress-mysql-b759dbb45-hqz6g   1/1     Running   1 (54m ago)   4h27m
wordpress-6c7bf9c4c9-dd5rp        1/1     Running   1 (54m ago)   4h27m
```

Copy files and change owner
```
kubectl cp uploads wordpress-6c7bf9c4c9-dd5rp:/var/www/html/wp-content/uploads
kubectl exec wordpress-6c7bf9c4c9-dd5rp -it -- chown -R 33:33 /var/www/html/wp-content/uploads
```

### Visit the WordPress admin page

https://wp.localnet/wp-admin/

Update WordPress Database

Login as admin

Visit Appearance / Themes and select the theme you want.
