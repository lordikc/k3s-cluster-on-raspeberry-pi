# Install a private Registry

## Certificate for the Registry

Generate a key:
```
openssl genrsa -out registry-pi.key 4096
```

Generate a Certificate Signing Request:
```
openssl req -sha512 -new \
    -subj "/C=FR/L=Paris/O=Lordikc/CN=registry-pi" \
    -key registry-pi.key \
    -out registry-pi.csr
```

Add the required extensions for a server and alternames if need be:
```
cat > v3.ext <<-EOF
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
extendedKeyUsage = serverAuth
subjectAltName = @alt_names

[alt_names]
DNS.1=registry-pi
DNS.2=registry-pi.localnet
EOF
```

Sign it with our CA to obtain the certificate:
```
openssl x509 -req -sha512 -days 3650 \
    -extfile v3.ext \
    -CA ca-ssl.crt -CAkey ca-ssl.key -CAcreateserial \
    -in registry-pi.csr \
    -out registry-pi.crt
```


Create the secret
```
kubectl create secret tls registry-tls --key="registry-pi.key" --cert="registry-pi.crt" -n kube-system
kubectl describe secret registry-tls -n kube-system
```

## Deploy the Registry image

Create the persistent volume claim for the registry:
```
kubectl apply -f registry-storage.yml 
kubectl get pvc  -n kube-system
```

Deploy the registry:
```
kubectl apply -f registry.yml
```

Create the registry service:
```
kubectl apply -f registry-service.yml
kubectl get svc -n kube-system
```

## Add registry-pi name resolution

Now we need to add registry-pi to DNS service or in all `/etc/hosts`.
Check if we have access to our Registry with:
```
curl https://registry-pi:5000/v2/_catalog
```
