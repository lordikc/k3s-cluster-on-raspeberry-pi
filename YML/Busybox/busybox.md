# Simple deployment with storage

## Deployment of busybox

Deploy busybox with a persistent volume claim mounted on /mnt.
```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: busybox
  labels:
    app: busybox
spec:
  replicas: 1
  selector:
    matchLabels:
      app: busybox
  template:
    metadata:
      labels:
        app: busybox
    spec:
      volumes:
      - name: busybox-vol
        persistentVolumeClaim:
          claimName: busybox-pvc
      containers:
        - image: busybox:latest
          name: busybox
          command: ["sleep"]
          args: ["999999"]
          volumeMounts:
          - name: busybox-vol
            mountPath: /mnt
```

```
kubectl get pods --selector=app=busybox
NAME                       READY   STATUS    RESTARTS   AGE
busybox-7846f79754-7k46g   0/1     Pending   0          13s
```

The pod is not yet ready because it waits for the PVC.


## Create the PVC

Create the persistent volume claim:
```yaml
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: busybox-pvc
  labels:
    app: busybox
spec:
  storageClassName: nfs-csi
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
```

Check the PVC:
```
kubectl get pvc  --selector=app=busybox
NAME          STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
busybox-pvc   Bound    pvc-0d1325f7-0ed2-4de4-8312-eceb06cba250   1Gi        RWO            nfs-csi        20s
```

The PVC is bound automatically thanks to the NFS CSI.
The pod should become ready.

## Access the pod

The pod name is given by this command:
```
kubectl get pods --selector=app=busybox -o=jsonpath="{range .items[0]}{.metadata.name}{'\n'}{end}"
```

Execute sh in the pod:
```
kubectl exec $(kubectl get pods --selector=app=busybox -o=jsonpath="{range .items[0]}{.metadata.name}{'\n'}{end}") -it -- sh
/ # mount | grep mnt
rp4-1:/data/pvc-0d1325f7-0ed2-4de4-8312-eceb06cba250 on /mnt type nfs4 (rw,relatime,vers=4.1,rsize=524288,wsize=524288,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,clientaddr=192.168.xx.xx,local_lock=none,addr=192.168.xx.xx)
```
The volume is mounted on /mnt.

