cfssl gencert -ca ~/Development/CA-cfssl/ca.pem -ca-key ~/Development/CA-cfssl/ca-key.pem -config ~/Development/CA-cfssl/cfssl.json -profile=peer certificate-nextcloud.json | cfssljson -bare nextcloud-peer

kubectl create secret tls nextcloud-tls --key="nc.key" --cert="nc.crt"
kubectl describe secret nextcloud-tls
