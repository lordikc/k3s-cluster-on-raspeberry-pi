# Introduction
In this tutorial we will create a namespace and a user with admin and access only to this namespace.

# Retrieve the CA from K3S
We will need both the CA for clients and the CA for the server.
```
scp root@rp4-1:/var/lib/rancher/k3s/server/tls/client-ca.crt .
scp root@rp4-1:/var/lib/rancher/k3s/server/tls/client-ca.key .
scp root@rp4-1:/var/lib/rancher/k3s/server/tls/server-ca.crt .
scp root@rp4-1:/var/lib/rancher/k3s/server/tls/server-ca.key .
```

Display the details of the CAs:
```
openssl x509 -in client-ca.crt -text
openssl x509 -in server-ca.crt -text
```

# Generate a certificate for a new user

Configuration of the certificate creation with CFSSL:
certificate.json
```
{
    "CN": "Sponge Bob",
    "key": {
	"algo": "rsa",
	"size": 2048
    }
}
```

Creation of the certificate with the client CA:
```
cfssl gencert -ca client-ca.crt -ca-key client-ca.key sponge.json | cfssljson -bare sponge
openssl x509 -in sponge.pem -text
```


# Create role and role binding

Create a namespace for the sponge user. Sponge will be admin of this namespace.

```
kubectl create ns sponges
```

Create the role and role binding.

```
kubectl -n sponges apply -f .\role.yaml
```


# Configure connection to the cluster

```
touch kube.config
export KUBECONFIG=$PWD/kube.config
kubectl config set-cluster cluster-pi --server=https://rp4-1:6443 \
--certificate-authority=server-ca.crt \
--embed-certs=true
kubectl config set-credentials sponge --client-certificate=sponge.pem --client-key=sponge-key.pem --embed-certs=true
kubectl config set-context pi-context --cluster=cluster-pi --user=sponge
kubectl config use-context pi-context
```

# Test

Deploy nginx:
```
kubectl create deploy nginx --image=nginx
```

Expose it as a service:
```
kubectl expose deploy nginx --port=80 --target-port=80 --name=nginx -n sponges
```

Create an ingress:

nginx-ingress.yml
```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: nginx-ingress
  namespace: sponges
  labels:
    app: nginx
spec:
  rules:
  - host: mynginx.com
    http:
      paths:
        - path: /
          pathType: Prefix
          backend:
            service:
              name: nginx
              port:
                number: 80
```

Create it:
```
kubectl apply -f nginx-ingress.yml
```

Resolv mynginx.com to the following address:
```
kubectl get ingress nginx-ingress  --output jsonpath='{.status.loadBalancer.ingress[0].ip}'
```

Check:
```
curl http://mynginx.com/
```

Create a PersistentVolumeClaim in this namepsace.
nginx-storage.yml:
```
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: nginx-pvc
  namespace: sponges
spec:
  storageClassName: nfs-csi
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
```

Create the PVC:
```
kubectl apply -f nginx-storage.yml
kubectl get pvc
```

Check that PVC is bound. Since sponge has only access to sponges namespace, it can't list the underlying PV:

```
$ kubectl get pv
Error from server (Forbidden): persistentvolumes is forbidden: User "Sponge Bob" cannot list resource "persistentvolumes" in API group "" at the cluster scope
```
