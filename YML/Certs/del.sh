#!/bin/bash

kubectl delete certificate -n cert-manager test-client
kubectl delete certificate -n cert-manager test-server
kubectl delete certificate test-client
kubectl delete certificate test-server
kubectl delete certificate -n cert-manager my-ca
kubectl delete Issuer selfsigned-issuer -n cert-manager
kubectl delete ClusterIssuer selfsigned-issuer
kubectl delete Issuer -n cert-manager my-ca-issuer
kubectl delete ClusterIssuer -n cert-manager my-ca-issuer
kubectl delete ingress echo-ingress -n cert-manager
kubectl delete service echo-service -n cert-manager
kubectl delete deployment echo -n cert-manager
