# Create a Cert Manager

## Introduction
In this tutorial you will install and configure an automatic certificate issuer with a private Certificate Authority.


## Add the repo and install the cert manager
```
helm repo add jetstack https://charts.jetstack.io
helm repo update

helm install \
    cert-manager jetstack/cert-manager \
    --namespace cert-manager \
	--create-namespace \
    --version v1.14.4 \
    --set installCRDs=true
```

## Create a self signed issuer

For a namespace and cluster wide.

```
cat > cert-manager-ss-issuer.yaml <<EOF
---
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  name: selfsigned-issuer
  namespace: cert-manager
spec:
  selfSigned: {}
---
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: selfsigned-issuer
spec:
  selfSigned: {}
EOF
kubectl apply -f cert-manager-ss-issuer.yaml
```

## Create our private CA

Create our CA:
```
cat > cert-manager-ca-cert.yaml <<EOF
---
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: my-ca
  namespace: cert-manager
spec:
  isCA: true
  commonName: my-ca
  subject:
    organizations:
      - Lordikc
    organizationalUnits:
      - Lordikc CA
  duration: 87600h
  secretName: my-ca-secret
  privateKey:
    algorithm: RSA
    size: 2048
  issuerRef:
    name: selfsigned-issuer
    kind: Issuer
    group: cert-manager.io
EOF
kubectl apply -f cert-manager-ca-cert.yaml
```

Check that the certificate was properly created.
```
kubectl -n cert-manager get certificate
```
NAME      READY   SECRET           AGE
my-ca   True    my-ca-secret   23s


Check that the secret was properly created.
```
kubectl -n cert-manager get secret my-ca-secret
```
NAME             TYPE                DATA   AGE
my-ca-secret   kubernetes.io/tls   3      39s

Retrieve the certificate and display it. The `my-ca.crt` file should be added to the trusted list of certificates.
```
kubectl -n cert-manager get secret my-ca-secret -o jsonpath='{.data.ca\.crt}' | base64 -d | tee my-ca.crt
openssl x509 -text -in my-ca.crt 
```


## Create an issuer for our new CA

```
cat > cert-manager-ca-issuer.yaml <<EOF
---
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  name: my-ca-issuer
  namespace: cert-manager
spec:
  ca:
    secretName: my-ca-secret
---
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: my-ca-issuer
spec:
  ca:
    secretName: my-ca-secret
EOF
kubectl apply -f cert-manager-ca-issuer.yaml
```

## First test

Issue our first certificate for server and client.
```
cat > test-server-cert.yaml <<EOF
---
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: test-server
  namespace: cert-manager
spec:
  secretName: test-server-tls
  isCA: false
  usages:
    - server auth
    - client auth
  dnsNames:
  - "test-server.test.svc.cluster.local"
  - "test-server"
  issuerRef:
    name: my-ca-issuer
---
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: test-client
  namespace: cert-manager
spec:
  secretName: test-client-tls
  isCA: false
  usages:
    - server auth
    - client auth
  dnsNames:
  - "test-client.test.svc.cluster.local"
  - "test-client"
  issuerRef:
    name: my-ca-issuer
EOF
kubectl apply -f test-server-cert.yaml
```

Check that the server certificate is valid against our CA.
```
openssl verify -CAfile \
<(kubectl get secret -n cert-manager my-ca-secret -o jsonpath='{.data.ca\.crt}' | base64 -d) \
<(kubectl get secret -n cert-manager test-server-tls -o jsonpath='{.data.tls\.crt}' | base64 -d)
```

## Deploy a test application

Create a test deployment of the echo service for checking that everything works well.
The annotation `cert-manager.io/issuer: my-ca-issuer` will trigger the creation of the corresponding certificate automatically.

```
cat > test-cert-manager.yaml <<EOF
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: test-cert-manager
  name: echo
  namespace: cert-manager
spec:
  replicas: 1
  selector:
    matchLabels:
      app: test-cert-manager
  template:
    metadata:
      labels:
        app: test-cert-manager
    spec:
      containers:
      - name: echo
        image: fdeantoni/echo-server
        imagePullPolicy: Always
        ports:
        - containerPort: 9000
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: test-cert-manager
  name: echo-service
  namespace: cert-manager
spec:
    selector:
      app: test-cert-manager
    ports:
    - name: http
      protocol: TCP
      port: 9000
      targetPort: 9000
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  labels:
    app: test-cert-manager
  name: echo-ingress
  namespace: cert-manager
  annotations:
    cert-manager.io/issuer: my-ca-issuer
spec:
    rules:
    - http:
        paths:
        - path: /test
          pathType: Prefix
          backend:
            service:
              name: echo-service
              port:
                number: 9000
    tls:
    - hosts:
        - test-cert-manager
      secretName: echo-cert
EOF
kubectl apply -f test-cert-manager.yaml
```

At this point test-cert-manager must resolv to the ingress controller. We can check that everythin is OK:
```
curl --cacert <(kubectl -n cert-manager get secret my-ca-secret -o jsonpath='{.data.ca\.crt}' | base64 -d) https://test-cert-manager/test
```
{"source":"10.42.0.6:46188","method":"GET","headers":[["host","test-cert-manager"],["user-agent","curl/7.88.1"],["accept","*/*"],["x-forwarded-for","10.42.2.0"],["x-forwarded-host","test-cert-manager"],["x-forwarded-port","443"],["x-forwarded-proto","https"],["x-forwarded-server","traefik-f4564c4f4-876dr"],["x-real-ip","10.42.2.0"],["accept-encoding","gzip"]],"path":"/test","server":"echo-57c9bf5d7-m8whm"}


## Uninstall
```
helm --namespace cert-manager delete cert-manager
kubectl delete namespace cert-manager -R
```
