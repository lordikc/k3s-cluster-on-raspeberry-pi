Creation of the certificate:
```
cfssl gencert -ca ~/Development/CA-cfssl/ca.pem -ca-key ~/Development/CA-cfssl/ca-key.pem -config ~/Development/CA-cfssl/cfssl.json -profile=peer certificate-gitlab.json | cfssljson -bare gitlab-peer
```

Create the secret
```
kubectl create secret tls gitlab-tls --key="gitlab-peer-key.pem" --cert="gitlab-peer.pem"
kubectl describe secret gitlab-tls
```
